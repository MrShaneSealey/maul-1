using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Using Fire();

public class AngleControll : MonoBehaviour
{

    [SerializeField] private float angleSpeed;
    [SerializeField] private float AngleLock; // around 15* degress is probs good

    //Ref: https://youtu.be/THnivyG0Mvo
    void Start()
    {
        
    }

    void Update()
    {
        transform.rotation *= Quaternion.Euler(0, 0, Input.GetAxis("Mouse Y") * angleSpeed * Time.fixedDeltaTime);

    }
}
