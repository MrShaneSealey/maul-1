using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyAi : MonoBehaviour
{

    //https://youtu.be/UjkSFoLxesw
    public NavMeshAgent agent;
    public float health = 100;
    public float damage = 10;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    public Vector3 movePoint;
    bool movePointSet;
    public float movePointRange;

    //attacking
    public float timeBetweenAtttacks;
    bool alreadyAttacked;

    //states (p = player)
    public float slightRange, attackRange;
    public bool pInSight, pAttack;

    public ParticleSystem muzzleFlash;
    public ParticleSystem muzzleSmoke;

    private void Awake()
    {
        player = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        pInSight = Physics.CheckSphere(transform.position, slightRange, whatIsPlayer);
        pAttack = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if (!pInSight && !pAttack) Patrolling();
        if (pInSight && !pAttack) ChasePlayer();
        if (pInSight && pAttack) AttackPlayer();
    }
    private void Patrolling()
    {
        if (!movePointSet) SearchMovePoint();

        if (movePointSet)
        {
            agent.SetDestination(movePoint);
        }

        Vector3 distanceToWalkPoint = transform.position - movePoint;

        if (distanceToWalkPoint.magnitude < 1f)
        {
            movePointSet = false;
        }
    }

    private void SearchMovePoint()
    {
        float randomZ = Random.Range(-movePointRange, movePointRange);
        float randomX = Random.Range(-movePointRange, movePointRange);

        movePoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        if (Physics.Raycast(movePoint, -transform.up, whatIsGround))
        {
            movePointSet = true;
        }
    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.position);
    }

    private void AttackPlayer()
    {
        muzzleFlash.Play();
        muzzleSmoke.Play();

        agent.SetDestination(transform.position);

        transform.LookAt(player);
        //var childTurret = transform.GetChild(9).gameObject;
        //var targetRotation = new Vector3(transform.position.x, transform.position.y , player.position.z);
        //childTurret.transform.LookAt(targetRotation);
        //new Vector3(transform.rotation.x, targetRotation.y, targetRotation.z)

        if (!alreadyAttacked)
        {

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAtttacks);
        }
    }

    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
    }
    void Die()
    {
        Destroy(gameObject);
        pController.instance.kills += 1;
    }
}


