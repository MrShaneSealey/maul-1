using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSys : MonoBehaviour
{
    public float health = 100;

    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
    }
    void Die()
    {
        Destroy(gameObject);
        pController.instance.kills += 1;
    }
}
