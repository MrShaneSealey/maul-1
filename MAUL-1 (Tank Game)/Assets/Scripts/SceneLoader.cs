using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneLoader
{

    public enum Scene
    {
        Tutorial,
    }

    public static void load(Scene scene)
    {
        SceneManager.LoadScene(scene.ToString());
    }
}


