using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class pController : MonoBehaviour
{
    private Rigidbody rb;

    [Header("Movement")]
    [SerializeField] private float turnSpeed;
    public float Speed;

    [Header("Health")]
    public float health = 100;
    public float kills = 0;

    [Header("Bullets")]
    public float ammo;

    [Header("Fire System")]
    public float damage = 20;
    public float range = 100;

    public Camera fpsCam;

    [Header("Particle System")]
    public ParticleSystem muzzleFlash;
    public ParticleSystem muzzleSmoke;
    public GameObject impactEffect;

    [Header("Audio")]

    public static AudioSource audioSrc;
    public AudioClip tankSound; 
    public AudioClip shootSound;


    public static pController instance;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        shootSound = Resources.Load<AudioClip>("Shoot Sound");
        tankSound = Resources.Load<AudioClip>("Tank Engine");

        audioSrc = GetComponent<AudioSource>();
        instance = this;
    }

    // Update is called once per frame
    void Update()
    { 
        transform.position += transform.rotation * new Vector3(0, 0, Input.GetAxis("Vertical")) * Speed * Time.fixedDeltaTime;
        transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Horizontal") * turnSpeed * Time.fixedDeltaTime ,0);

        if (Input.GetButtonDown("Vertical") && Input.GetButtonDown("Horizontal"))
        {
            PlaySound(tankSound);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Fire();
            PlaySound(shootSound);
            muzzleFlash.Play();
            muzzleSmoke.Play();

        }
    }

    private void PlaySound(AudioClip clip)
    {
        audioSrc.PlayOneShot(shootSound);
        audioSrc.PlayOneShot(tankSound);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.CompareTag("Health"))
        {
            Debug.Log("Free Health");
            health += 25;
        }

        if (collider.transform.CompareTag("Armor"))
        {
            Debug.Log("Free Armor");
            //armor = 1;
        }

        if (collider.transform.CompareTag("Ammo"))
        {
            Debug.Log("Free Bullets");
            ammo += 2;
        }

    }
    void Fire()
    {
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            TargetSys target = hit.transform.GetComponent<TargetSys>();
            if (target != null)
            {
                target.TakeDamage(damage);
            }
            
            //Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
        }
    }
}

