using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadController : MonoBehaviour
{
    [Header("Rotation")]
    [SerializeField] private float turnSpeed;

    [Header("Camera")]
    [SerializeField] private float cameraDistance;
    [SerializeField] private float cameraHeight;
    [SerializeField] private Transform cameraTransform;

    public static CursorLockMode lockState;
    //Ref: https://docs.unity3d.com/ScriptReference/Cursor-lockState.html

    // Start is called before the first frame update
    void Start()
    {
        cameraTransform = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.CapsLock))
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        transform.rotation *= Quaternion.Euler(0, 0, Input.GetAxis("Mouse X") * turnSpeed * Time.fixedDeltaTime);

        cameraTransform.position = transform.position + transform.rotation * new Vector3(0, cameraHeight, cameraDistance);
        cameraTransform.rotation = Quaternion.LookRotation(transform.position - cameraTransform.position);

    }

}